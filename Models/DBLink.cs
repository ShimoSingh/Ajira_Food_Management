﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Ajira_Food_Management.Models
{
    public class DBLink
    {
        static string connstring = "Server=127.0.0.1;Port=5432;Database=food_management;User Id=food_management;password=asas";
            // Making connection with Npgsql provider
        static NpgsqlConnection conn = new NpgsqlConnection(connstring);
        public static void addEmployee(string employeeName)
        {
            conn.Open();
            string sql = "insert into employees(name) values('" + employeeName + "')";
            NpgsqlCommand da = new NpgsqlCommand(sql, conn);
            da.ExecuteNonQuery();
            conn.Close();
        }
        public static void deleteEntry(string itemName,string itemType)
        {
            conn.Open();
            string sql = "delete from "+itemType+" where name='" + itemName + "'";
            NpgsqlCommand da = new NpgsqlCommand(sql, conn);
            da.ExecuteNonQuery();
            conn.Close();
        }
        public static void addFoodItem(string foodName)
        {
            conn.Open();
            string sql = "insert into food(name) values('" + foodName + "')";
            NpgsqlCommand da = new NpgsqlCommand(sql, conn);
            da.ExecuteNonQuery();
            conn.Close();
        }
        public static string[] getNameList(string listName)
        {
            DataTable dt = new DataTable();
            conn.Open();
            string sql = "select name from " + listName;
            NpgsqlDataAdapter da = new NpgsqlDataAdapter(sql, conn);
            da.Fill(dt);
            string[] nameList = new string[dt.Rows.Count]; 
            for(int i=0;i<nameList.Length;i++)
            {
                nameList[i] = dt.Rows[i][0].ToString();
            }
            conn.Close();
            return nameList;
        }
    }
}