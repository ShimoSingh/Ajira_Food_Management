﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ajira_Food_Management.Models
{
    public class RequestManager
    {
        public static SuggestionList getSuggestionList(string listName)
        {
            return new SuggestionList(listName);
        }
        public static string manageOperation(JObject json)
        {
            List<string> query = new List<string>();
            foreach (var x in json)
            {
                query.Add((string)x.Value);
            }
            if (query.ElementAt(1).Equals("add"))
                addToDB(query);
            else
            {
                DBLink.deleteEntry(query.ElementAt(2), query.ElementAt(0));
            }
            return "true";
        }
        private static void addToDB(List<string> query)
        {
            if (query.ElementAt(0).Equals("employees"))
                DBLink.addEmployee(query.ElementAt(2));
            else if (query.ElementAt(0).Equals("food"))
                DBLink.addFoodItem(query.ElementAt(2));
        }
    }
    public class SuggestionList
    {
        public string[] suggestionList;
        public SuggestionList(string listName)
        {
            suggestionList= DBLink.getNameList(listName);
        }
    }
}