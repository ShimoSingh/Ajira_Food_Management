window.onload=init;
var manage={type:null,operation:null,name:null};
var suggestionList;
var Response;
function init()
{
	var empForm=document.querySelector("#employee-button");
	empForm.addEventListener('click',showEmployeeForm);
	empForm=document.querySelector("#food-button");
	empForm.addEventListener('click',showFoodForm);
	empForm=document.querySelector("#cancelForm");
	empForm.addEventListener('click',closeManageForm);
	empForm=document.querySelector("#doneForm");
	empForm.addEventListener('click',submitManageForm);
	document.querySelector("#objectAddButton").addEventListener('click',setManageToAdd);
	document.querySelector("#objectRemoveButton").addEventListener('click',setManageToRemove);
}
function showSuggestions()
{
	var inp=document.querySelector("#formInput");
var a, b, i, val = inp.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      var currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      inp.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < suggestionList.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        if (suggestionList[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + suggestionList[i].substr(0, val.length) + "</strong>";
          b.innerHTML += suggestionList[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + suggestionList[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
              b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
              document.querySelector("#formInput").value = this.getElementsByTagName("input")[0].value;
              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
          });
          a.appendChild(b);
        }
      }	
}
function closeAllLists(elmnt) {
        var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      x[i].parentNode.removeChild(x[i]);
	}
}
function submitManageForm()
{
	manage.name=document.querySelector("#formInput").value;
	if(isManagePostValid())
	{
		postManageForm();
		closeManageForm();
	}
	else
	{
		var messagePart=document.querySelector("#errorMessage");
	  messagePart.innerHTML='Invalid Input';
	  messagePart.style.backgroundColor="rgba(255,102,102,0.7)";
	}

}
function isManagePostValid()
{
	return manage.type&&manage.operation&&manage.name;
}
function setManageToAdd(evt)
{
	manage.operation="add";
	evt.target.style.backgroundColor="blue";
	document.querySelector("#objectRemoveButton").style.backgroundColor="";
	document.querySelector("#formInput").removeEventListener('input',showSuggestions);
}
function setManageToRemove(evt)
{
	manage.operation="remove";
	evt.target.style.backgroundColor="blue";
	document.querySelector("#objectAddButton").style.backgroundColor="";
	document.querySelector("#formInput").addEventListener('input',showSuggestions);
}
function showEmployeeForm(evt)
{
	loadSuggestionList("employees");
	manage.type="employees";
	document.querySelector("#manageHead").innerHTML="Manage Employees";
	var form=document.querySelector("#form");	
	form.style.display="block";
	form.style.overflow="hidden";
	}
function postManageForm()
{
	var queryURL = "http://localhost:64183/foodapi/data";
    
    var xhr = new XMLHttpRequest();
    xhr.open('POST', queryURL, true);
	xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    // called when the response is arrived
    xhr.onload = function(e) {
      var jsonResponse = xhr.response;

      // turn the response into a JavaScript object
      var Response = JSON.parse(jsonResponse);
	  displayRequestMessage(Response);
	 }
    
    // in case of error
    xhr.onerror = function(err) {
      console.log("Error: " + err);
    }
    
    // sends the request
    xhr.send(JSON.stringify(manage));

}
function displayRequestMessage(response)
{
	if(response=='true')
	{
		var messagePart=document.querySelector("#requestMessage");
		messagePart.innerHTML='Done';
		messagePart.style.backgroundColor="rgba(34,139,34,0.7)";
	}
	else
	{
		var messagePart=document.querySelector("#requestMessage");
	  messagePart.innerHTML='Something Went Wrong';
	  messagePart.style.backgroundColor="rgba(255,102,102,0.7)";
	}
}
function loadSuggestionList(id)
{
	var queryURL = "http://localhost:64183/foodapi/data?id="+id;
    
    var xhr = new XMLHttpRequest();
    xhr.open('GET', queryURL, true);

    // called when the response is arrived
    xhr.onload = function(e) {
      var jsonResponse = xhr.response;

      // turn the response into a JavaScript object
      suggestionList = JSON.parse(jsonResponse).suggestionList;
    }
    
    // in case of error
    xhr.onerror = function(err) {
      console.log("Error: " + err);
    }
    
    // sends the request
    xhr.send();
}
function showFoodForm(evt)
{
	loadSuggestionList("food");
	manage.type="food";
	document.querySelector("#manageHead").innerHTML="Manage Food Items";
	var form=document.querySelector("#form");	
	form.style.display="block";
	form.style.overflow="hidden";
}
function closeManageForm()
{
	var form=document.querySelector("#form");	
	form.style.display="none";
}