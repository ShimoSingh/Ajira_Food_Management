﻿using Ajira_Food_Management.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace Ajira_Food_Management.Controllers
{
    public class DataController : ApiController
    {
        // GET /values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        // GET api/values/5
        public JObject Get(string id)
        {
            var result = RequestManager.getSuggestionList(id);
            JObject json= JObject.FromObject(result);
            return json;
        }

        // POST api/values
        public string Post(JObject value)
        {
            var result = RequestManager.manageOperation(value);
            return result;
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
